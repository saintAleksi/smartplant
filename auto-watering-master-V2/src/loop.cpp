/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 *
 * The Arduino loop function which is called in a infinite loop.
 */

#include "loop.h"

#include "actions.h"
#include "settings.h"
#include "rh.h"

// *****************
//  Menu LCD & Rotary encoder 
// *****************
#include <CMBMenu.hpp>
#include <LiquidCrystal_I2C.h>

// *****************
//  Menu LCD & Rotary encoder - END
// *****************

bool adcOn = false;

void loop () {
  
  // variable to switch between different modes
  unsigned short switchMode = 1;

  if(switchMode == 1){
    unsigned long now = millis();    
  
    // temperature sensor code only if TEMP_SENSOR_TYPE is not 0
    #if TEMP_SENSOR_TYPE != 0
      // check if we need to read from the temperature sensor
      if (checkTime(now, humSensorNextReadTime)) {
        // read from the sensor using the correct method for the sensor type
        
  
        bool sensorReadOk = false;
  
        #if TEMP_SENSOR_TYPE == 11 || TEMP_SENSOR_TYPE == 12 || TEMP_SENSOR_TYPE == 22
          // DHT sensor
          #if TEMP_SENSOR_TYPE == 11
            int dhtResult = dhtSensor.read11(TEMP_SENSOR_PIN);
          #elif TEMP_SENSOR_TYPE == 12
            int dhtResult = dhtSensor.read12(TEMP_SENSOR_PIN);
          #elif TEMP_SENSOR_TYPE == 22
            int dhtResult = dhtSensor.read22(TEMP_SENSOR_PIN);
          #endif
          
  
          // get the values
          temperature = dhtSensor.getTemperature();
          humidity = dhtSensor.getHumidity();
          Serial.println("temperature:");
          Serial.println(temperature);

  
          // check the result and also if the values are plausible
          if (dhtResult == DHTLIB_OK
            && humidity >= 0 && humidity <= 100
            && temperature >= -50 && temperature <= 100) {
            // sensor read ok
            sensorReadOk = true;
          } else {
            temperature = -99;
            humidity = -99;
          }
          Serial.println("humidity:");
          Serial.println(humidity);
  
        #elif TEMP_SENSOR_TYPE == 1820
          // DS18x20 sensor
          ds1820.requestTemperatures();
          temperature = ds1820.getTempCByIndex(0);
  
          if (temperature != DEVICE_DISCONNECTED_C) {
            sensorReadOk = true;
          } else {
            temperature = -99;
          }
  
        #else
          #error TEMP_SENSOR_TYPE must be 11, 12, 22, 1820 or 0!
        #endif
  

        //***********************************temp changed to hum******************************************
        if (sensorReadOk) {
          // check temperature switch
          if (humSwitchTriggerValueLow != 0.0 && humSwitchTriggerValueHigh != 0.0) {
            // automatic switching enabled
            if (!humSwitchOn && (
              (humidity <= humSwitchTriggerValueHigh && !settings.humSwitchInverted)
              || (humidity >= humSwitchTriggerValueLow && settings.humSwitchInverted)
            )) {
              // turn on the fan switch to lower humidity
              digitalWrite(HUM_SWITCH_PIN, HIGH);
              humSwitchOn = true;
            } else if (humSwitchOn && (
              (humidity >= humSwitchTriggerValueLow && !settings.humSwitchInverted)
              || (humidity <= humSwitchTriggerValueHigh && settings.humSwitchInverted)
            )) {
              // turn off fan for higher Humidity
              digitalWrite(HUM_SWITCH_PIN, LOW);
              humSwitchOn = false;
            }
          }
          // send data;
          rhSendData(RH_MSG_TEMP_SENSOR_DATA);
        } 
        else {
          // sensor read error
          blinkCode(BLINK_CODE_TEMP_SENSOR_ERROR);
        }
  
        // calc next dht read time
        humSensorNextReadTime = now + ((uint32_t)settings.humSensorInterval * 1000);
      }
    #endif
        
    // check if we need to turn on the adc and sensors 1 second before reading the adc values
    // this is to give the sensors and the adc some time to reach a stable level
    if (adcOn == false && checkTime(now, (adcNextReadTime - 1000))) {
        //Serial.println("Adc turned on");
        // enable the adc
        ADCSRA |= (1<<ADEN);
  
        // enable the sensors
        digitalWrite(SENSORS_ACTIVE_PIN, HIGH);
  
        // set marker that the adc is on
        adcOn = true;
    }
  
    // check if we need to read the adc values
    if (checkTime(now, adcNextReadTime)) {
      //Serial.println("ADC Reading");
      // only read sensors if not pause
      if (!pauseAutomatic) {
        // read adc values and check if we need to turn on some channels
        for (uint8_t chan = 0; chan < 4; chan++) {
          if (settings.channelEnabled[chan]) {
            // read the adc value of the channel
            adcValues[chan] = analogRead(sensorAdcPins[chan]);
            // check trigger value
            if (adcValues[chan] >= settings.adcTriggerValue[chan]) {
              // set marker to turn the channel on
              channelTurnOn[chan] = true;
            }
          }
        }
        // send RadioHead message (send adc check is done later...)
        rhSendData(RH_MSG_SENSOR_VALUES);
      }
  
      // disable the sensors
      digitalWrite(SENSORS_ACTIVE_PIN, LOW);
  
      // read battery voltage
      #if BAT_ENABLED == 1
        batteryRaw = analogRead(BATTERY_ADC);
        rhSendData(RH_MSG_BATTERY);
      #endif

      //*************Code for photo related stuff is HERE ****************
      
      if (currentDayStart >= 86400000){
        resetDayTime(now);
        Serial.println("Daytime is reset");
      }
      if (checkSunTimeReachable(now, settings.lightMinTime) && checkSun()){
        
        
        //if SunTime is reachable and Sun is shining: add Suntime and Turn Light of
        /*
        Serial.print("adding ");
        Serial.print(settings.checkInterval);
        Serial.println(" to lightTodayTime");
        */
        lightTodayTime = lightTodayTime + settings.checkInterval;
        digitalWrite(LIGHT_PIN, LOW);

        
      }
      /*
      Serial.print("lightMinTime in min: ");
      Serial.println(settings.lightMinTime);
      */
      if (!checkSunTimeReachable(now, settings.lightMinTime)){
      
        //if SunTime is not reachable LIGHT should be turned on, if there is no sunlight
        if (!checkSun()){
          digitalWrite(LIGHT_PIN, HIGH);
        }
        else{
          lightTodayTime = lightTodayTime + settings.checkInterval;
          digitalWrite(LIGHT_PIN, LOW);
        }
      }
      //**********************************************************************************
      
  
      // disable the adc
      ADCSRA &= ~(1<<ADEN);
  
      // set marker that the adc is off
      adcOn = false;
  
      // calc next adc read time
      adcNextReadTime = now + ((uint32_t)settings.checkInterval * 1000);
    }
  
    for (uint8_t chan = 0; chan < 4; chan++) {
      if (settings.channelEnabled[chan]) {
        // check turn off
        if (channelOn[chan] == true && checkTime(now, channelTurnOffTime[chan])) {
          turnValveOff(chan);
        }
        // check turn on
        else if (channelOn[chan] == false && channelTurnOn[chan] == true) {
          if (turnValveOn(chan)) {
            // calc the turn off time
            channelTurnOffTime[chan] = now + ((uint32_t)settings.wateringTime[chan] * 1000);
            // reset the turn on indicator
            channelTurnOn[chan] = false;
          }
        }
      }
    }
  
    // receive RadioHead messages
    rhRecv();
  }

  // if switchmode = 0, mode is set to manual and the user can input values using the rotary encoder and LCD
  if(switchMode == 0){

    int fid = 0;                         // function ID
    const char* info;                    // info text from menu
    bool layerChanged=false;             // go to deeper or upper layer?
    SIG_PB = digitalRead(button.pin);
    unsigned long currentMillis = millis();
    
    // check how long has it been since last user input
    // if longer than set interval, turn backlight off to save power
    if (currentMillis - prevMi >= inter){
      prevMi = currentMillis;
      lcd.noBacklight();
      counter = 0;
    }

    // if changing a value, display current array value on LCD
    // LCD doesnt work well with interrupts so thats why we are doing the displaying here
    if (modVal == 1){
      prevMi = currentMillis;           // update so screen doesnt enter power save mode

      if (refreshVal == 1){
      lcd.clear();
      lcd.println(arrPtr[rotate]);
      refreshVal = 0;                   // swap to false so we wait for next input
      }                                 // and wont refresh the screen continuously

      if (SIG_PB == LOW && currentMillis - previousMillis >= interval){
        valPtr = &arrPtr[rotate];
        ExitModify();
      }
      Serial.println(*valPtr);
    }

    //// ** menu **
    //// print/update menu
    //// and get current function ID "fid"
    if (counter == 1){
        fid = g_Menu.getInfo(info);
        printMenuEntry(info);
        lcd.backlight();
        prevMi = currentMillis;
        counter = 0;
    }

    // check if pb pressed and then update mode info
    if (currentMillis - previousMillis >= interval){
      previousMillis = currentMillis;

      if(SIG_PB == LOW){
      fid = g_Menu.getInfo(info);
      Serial.println("pressed");
      g_Menu.enter(layerChanged);
      counter = 1;
      }

    }

    if ((0 != fid) && (counter == 1) && (!layerChanged)) {
      switch (fid) {
        case MenuReturn:
          MReturn();
          break;
        case MenuModify:
        
          break;
        case MenuOk:
          // call EEPROM save function and set switchmode to = 0 -> ie automatic
          break;
        case MenuBarReturn:
          MReturn();
          break;
        case MenuBarA:
          BarA();
          break;
        case MenuTest1:
          Test1();
          break;
        case MenuTest2:
          Test2();
          break;
        default:
          break;
      }
    }
  }

  }
}
