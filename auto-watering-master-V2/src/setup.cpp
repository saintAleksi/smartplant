/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 *
 * The Arduino setup function which is called once on startup.
 */

#include "setup.h"

#include <EEPROM.h>
#include "actions.h"
#include "pcint.h"
#include "rh.h"
#include "settings.h"

void setup () {

  // setup the pins
  pinMode(VALVE_0_PIN, OUTPUT);
  pinMode(VALVE_1_PIN, OUTPUT);
  pinMode(VALVE_2_PIN, OUTPUT);
  pinMode(VALVE_3_PIN, OUTPUT);
  pinMode(VALVE_0_BUTTON_PIN, INPUT_PULLUP);
  pinMode(VALVE_1_BUTTON_PIN, INPUT_PULLUP);
  pinMode(VALVE_2_BUTTON_PIN, INPUT_PULLUP);
  pinMode(VALVE_3_BUTTON_PIN, INPUT_PULLUP);
  pinMode(SENSORS_ACTIVE_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(EEPROM_RESET_PIN, INPUT_PULLUP);
  pinMode(HUM_SWITCH_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(PUMP_PIN, OUTPUT);

  // encoder pins 
  attachInterrupt(ENCODER_A_PIN, A_CHANGE, CHANGE);
  SIG_B = digitalRead(ENCODER_B_PIN);                 // current state of B
  SIG_A = SIG_B > 0 ? 0 : 1;                          // let them be different
  pinMode(button.pin, INPUT_PULLUP);                  // declare pushbutton as input
  pinMode(ENCODER_5V_PIN, OUTPUT);
  pinMode(ENCODER_GND_PIN, OUTPUT);
  digitalWrite(ENCODER_5V_PIN, HIGH);
  digitalWrite(ENCODER_GND_PIN, LOW);

  // initialize the lcd 
  lcd.init();

  // ** menu **
  // add nodes to menu (layer, string, function ID)
  g_Menu.addNode(0, MenuAdc_pc , MenuAdc);
  g_Menu.addNode(1, MenuAdcReturn_pc, MenuAdcReturn);
  g_Menu.addNode(1, MenuAdcModify_pc, MenuAdcModify);

  g_Menu.addNode(0, MenuWater_pc, MenuWater);
  g_Menu.addNode(1, MenuWaterReturn_pc, MenuWaterReturn);
  g_Menu.addNode(1, MenuWaterModify_pc, MenuWaterModify);

  g_Menu.addNode(0, MenuHum_pc, MenuHumidity);
  g_Menu.addNode(1, MenuHumReturn_pc, MenuHumReturn);
  g_Menu.addNode(1, MenuHumModify_pc, MenuHumModify);

  g_Menu.addNode(0, MenuLightTime_pc, MenuLightTime);
  g_Menu.addNode(1, MenuLightTimeReturn_pc, MenuLightTimeReturn);
  g_Menu.addNode(1, MenuLightTimeModify_pc, MenuLightTimeModify);

  g_Menu.addNode(0, MenuOk_pc, MenuOk);


  // ** menu **
  // build menu and print menu
  // (see terminal for output)
  const char* info;
  g_Menu.buildMenu(info);
  g_Menu.printMenu();

  // ** menu **
  // print current menu entry
   printMenuEntry(info);

  // ****************** END

  // blink the LED to indicate starting
  blinkCode(BLINK_LONG);

  // all channels are off while starting
  for (uint8_t chan = 0; chan < 4; chan++) {
    channelOn[chan] = false;
    channelTurnOn[chan] = false;
    channelTurnOffTime[chan] = 0;
    digitalWrite(valvePins[chan], LOW);
    digitalWrite(PUMP_PIN, LOW);
  }
  digitalWrite(SENSORS_ACTIVE_PIN, LOW);
  digitalWrite(HUM_SWITCH_PIN, LOW);
  pauseAutomatic = false;

  // enable PCINT for the buttons
  attachPCINT(digitalPinToPCINT(VALVE_0_BUTTON_PIN), handlePcintButton0, FALLING);
  attachPCINT(digitalPinToPCINT(VALVE_1_BUTTON_PIN), handlePcintButton1, FALLING);
  attachPCINT(digitalPinToPCINT(VALVE_2_BUTTON_PIN), handlePcintButton2, FALLING);
  attachPCINT(digitalPinToPCINT(VALVE_3_BUTTON_PIN), handlePcintButton3, FALLING);

  // setup the ADC
  ADMUX =
    (1 << ADLAR) | // left shift result
    (0 << REFS1) | // Sets ref. voltage to VCC, bit 1
    (0 << REFS0) | // Sets ref. voltage to VCC, bit 0
    (0 << MUX3)  | // use ADC2 as default input, MUX bit 3
    (0 << MUX2)  | // use ADC2 as default input, MUX bit 2
    (1 << MUX1)  | // use ADC2 as default input, MUX bit 1
    (0 << MUX0);   // use ADC2 as default input, MUX bit 0
  ADCSRA =
    (1 << ADEN)  | // enable ADC
    (1 << ADPS2) | // set prescaler to 64, bit 2
    (1 << ADPS1) | // set prescaler to 64, bit 1
    (0 << ADPS0);  // set prescaler to 64, bit 0

  // disable ADC for powersaving
  ADCSRA &= ~(1<<ADEN);

  // disable analog comperator for powersaving
  ACSR |= (1<<ACD);

  // need to reset the config?

  if (digitalRead(EEPROM_RESET_PIN) == HIGH || EEPROM.read(EEPROM_ADDR_VERSION) != EEPROM_VERSION) {
    // set default config
    loadDefaultSettings();
    Serial.println("loadDefaultSettings");

    // save config to eeprom
    saveSettings();

    // write the current config data model version to the eeprom
    EEPROM.update(EEPROM_ADDR_VERSION, EEPROM_VERSION);

    // blink LED while eeprom reset button is pressed
    while (digitalRead(EEPROM_RESET_PIN) == LOW) {
      blinkCode(BLINK_SHORT, BLINK_SHORT, BLINK_SHORT);
      delay(1000);
    }
  } else {
    // read config from eeprom
    loadSettings();
  }

  // init RadioHead
  rhInit();

  // init temperature sensor if necessary
#if TEMP_SENSOR_TYPE == 1820
  #if DS1820_RESOLUTION != 9 && DS1820_RESOLUTION != 10 && DS1820_RESOLUTION != 11 && DS1820_RESOLUTION != 12
    #error DS1820_RESOLUTION must be 9, 10, 11 or 12!
  #endif

  ds1820.begin();
  ds1820.setResolution(DS1820_RESOLUTION);
#endif

  // calc adc and temperature sensor next read time, 5/10 seconds from now
  // temperature sensor read is 5 seconds before adc read to avoid both readings at the same time
  humSensorNextReadTime = millis() + 5000;
  adcNextReadTime = millis() + 10000;

  // send RadioHead start message
  rhSendData(RH_MSG_START);
  Serial.begin(9600);
}
