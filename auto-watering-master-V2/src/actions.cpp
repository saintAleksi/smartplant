/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 *
 * Functions to do some actions.
 */

#include "actions.h"

#include "rh.h"

/**
 * Blink the LED with a tripple blink code.
 * @param t1 Time 1 in ms.
 * @param t2 Time 2 in ms (optional).
 * @param t3 Time 3 in ms (optional).
 */
void blinkCode (uint16_t t1, uint16_t t2, uint16_t t3) {
  digitalWrite(LED_PIN, HIGH);
  delay(t1);
  digitalWrite(LED_PIN, LOW);
  if (t2 > 0) {
    delay(100);
    digitalWrite(LED_PIN, HIGH);
    delay(t2);
    digitalWrite(LED_PIN, LOW);
  }
  if (t3 > 0) {
    delay(100);
    digitalWrite(LED_PIN, HIGH);
    delay(t3);
    digitalWrite(LED_PIN, LOW);
  }
}

/**
 * Turns the valve of the given channel on if no other valve is currently turned on.
 *
 * Returns `true` if the valve is turned on, `false` if it is not turned on
 * because an other valve is already on.
 */
bool turnValveOn (uint8_t chan) {
  // check if we can turn on
  if (channelOn[0] || channelOn[1] || channelOn[2] || channelOn[3]) {
    // one channel is on
    return false;
  }

  // turn the valve pin and pump on
  // pump was added to the system
  digitalWrite(valvePins[chan], HIGH);
  digitalWrite(PUMP_PIN, HIGH);

  // set marker that this channel is on
  channelOn[chan] = true;

  // send RadioHead message
  rhSendData(RH_MSG_CHANNEL_STATE);

  return true;
}

/**
 * Turns the valve of the given channel off.
 */
void turnValveOff (uint8_t chan) {
  // turn the valve pin and pump off
  // pump was added to the system
  digitalWrite(valvePins[chan], LOW);
  digitalWrite(PUMP_PIN, LOW);

  // set marker that this channel is off
  channelOn[chan] = false;

  // send RadioHead message
  rhSendData(RH_MSG_CHANNEL_STATE);
}


/*
//********************************* THIS PART WAS ADDED ****************************
  functions which are used to monitor the sunlight and turning on the lights according
  to a certain time per day as saved in settings
*/
bool checkSunTimeReachable(unsigned long currenttime, uint16_t goalTime){
  // Check if Suntime is reachable with the resting daytime
  signed long restDay; 
  int16_t sunTimeNeeded;
    
  restDay = 86400000 -(currenttime - currentDayStart);
  sunTimeNeeded = (goalTime*60)- lightTodayTime;
  /*
  Serial.print("currentDayStart in s:");
  Serial.println(currentDayStart/1000);
  Serial.print("Now in s: ");
  Serial.println(currenttime/1000);
  Serial.print("Rest of the Day in s: ");
  Serial.println(restDay);
  Serial.print("Sun time needed in s: ");
  Serial.println(sunTimeNeeded);
  */
  

  
  if (restDay <= ((goalTime*60)-lightTodayTime)){
    
    //Serial.println("Suntime is not reachable");
    return false;
  }
  else{
    //Serial.println("Suntime is reachable");
    return true;
  }
}

void resetDayTime(unsigned long resetvalue){
  // in main loop now can be used as reset value --> millis() shouldn't be called several times
  // function that resets daytime to a given number and sets the lighttime of that day to 0
  currentDayStart = resetvalue;
  lightTodayTime = 0;
}

bool checkSun(){
  //CAREFUL function can only be called, if the ADC is turned on (inside ADC checktime)
  uint8_t currentLight;
  currentLight = analogRead(PHOTO_ADC);
  Serial.print("Photovalue: ");
  Serial.println(currentLight);
  if (currentLight <= settings.lightTriggerValue){
    return true;
  }
  else{
    return false;
  }
}
