/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 *
 * Global definition of some variables/constants.
 */

#include "globals.h"

// global variables
Settings settings;

volatile bool channelTurnOn[4]; // volatile to use this inside a ISR
volatile unsigned long channelTurnOffTime[4];
unsigned long adcNextReadTime;
unsigned long humSensorNextReadTime;

// variables added for monitoring the light during a period of one Day
uint16_t currentDayStart;
uint16_t lightTodayTime;

volatile bool channelOn[4];
uint16_t adcValues[4] = {0, 0, 0, 0};
float temperature = -99;
float humidity = -99;

// *****************
//  Menu LCD & Rotary encoder 
// *****************
LiquidCrystal_I2C lcd(0x27,20,4);     // set the LCD address to 0x27 for a 16 chars and 2 line display
CMBMenu<100> g_Menu;

int pulseCount;                       // rotation step count
int SIG_A = 0;                        // pin A output
int SIG_B = 0;                        // pin B output
int SIG_PB = 0;                       // push button output
int lastSIG_A = 0;                    // last state of SIG_A
int lastSIG_B = 0;                    // last state of SIG_B

int counter = 0;                      // if 1 -> encoder turned or pressed -> refresh lcd
unsigned long previousMillis = 0;        
const long interval = 405;            // check if pb pressed
unsigned long prevMi = 0;
const long inter = 30000;             // time before turn backlight off

const unsigned long  longPress = 5000;// pb needs to be pressed for this long before mode change
int switchmode = 0;                   // if 1 then mode = manual and screen turns on, 0 = automatic so we download hardcoded default settings
int refreshVal = 0;                   // controls LCD refresh when selecting values for sensors

unsigned short modVal = 0;

int rotate = 0;

// arrays to rotate through with preset values when manually changing settings
int adcTriggerValues[max] = {1,2,3,4,5,6,7,8,9,10};
int wateringTimeValues[max] = {10,20,30,40,50,60,70,80,90,100};
int humidityTriggerValues[max] = {50,100,150,200,250,300,350,400,450,500};
int lightTimeValues[max] = {1,2,3,4,5,6,7,8,9,10};

// ********************************************
// RotateLeft   // anti-clockwise rotation through the array
// ********************************************
void RotateLeft(int array[]){

  if (rotate < max && rotate >= 0){
    rotate--;
  }else{
    rotate = 9;
  }
}

// ********************************************
// RotateRight   // clockwise rotation through the array
// ********************************************
void RotateRight(int array[]){

  if (rotate < max && rotate >= 0){
    rotate++;
  }else{
    rotate = 0;
  }
}

// ********************************************
// ExitModify   // exits the current modification & changes modVal to 0 -> rotation input is restored to main menu cycling  
// ********************************************
void ExitModify(){
  g_Menu.exit();
  modVal = 0;
}

// ********************************************
// MReturn  -> return to previous menu layer
// ********************************************
void MReturn(){
  g_Menu.exit();
}

// ********************************************
// MenuAdcMod    // set mod value to 1 -> encoder rotation is captured by array loop
// assign corresponding pointers to their respective values
// ********************************************
void MenuAdcMod(){
  modVal = 1;
  arrPtr = adcTriggerValues;
  settings.adcTriggerValue[1] = *valPtr;   // <----------- need to specify channel
}

// ********************************************
// MenuWaterMod  //
// ********************************************
void MenuWaterMod(){
  modVal = 1;
  arrPtr = wateringTimeValues;
  settings.wateringTime[1] = *valPtr;     // <----------- need to specify channel
}

// ********************************************
// MenuHumMod   //
// ********************************************
void MenuHumMod(){
  modVal = 1;
  arrPtr = humidityTriggerValues;
  settings.humSwitchTriggerValue = *valPtr;
}

// ********************************************
// MenuLightMod //
// ********************************************
void MenuLightMod(){
  modVal = 1;
  arrPtr = lightTimeValues;
  settings.lightMinTime = *valPtr;
}

// ********************************************
// SaveSettings //  save user setup to EEPROM and return to automatic mode
// ********************************************
void SaveSettings(){
  switchmode = 1;

}

// ********************************************
// ** menu **
// printMenuEntry
// ********************************************
void printMenuEntry(const char* f_Info){
  String info_s;
  MBHelper::stringFromPgm(f_Info, info_s);

  // when using LCD: add/replace here code to
  // display info on LCD
  Serial.println("----------------");
  lcd.clear();
  lcd.println(info_s);
  Serial.println("----------------");
}

// *****************
//  Menu LCD & Rotary encoder - END
// *****************

#if BAT_ENABLED == 1
  uint16_t batteryRaw;
#endif

bool humSwitchOn = false;
float humSwitchTriggerValueHigh = 32;
float humSwitchTriggerValueLow = 28;

bool pauseAutomatic;

#if TEMP_SENSOR_TYPE == 11 || TEMP_SENSOR_TYPE == 12 || TEMP_SENSOR_TYPE == 22
  DHTStable dhtSensor;
#elif TEMP_SENSOR_TYPE == 1820
  OneWire oneWire(TEMP_SENSOR_PIN);
  DallasTemperature ds1820(&oneWire);
#endif
