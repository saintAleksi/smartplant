/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 */
#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#include "config.h"

#include <Arduino.h>
#include <SPI.h>
#if TEMP_SENSOR_TYPE == 11 || TEMP_SENSOR_TYPE == 12 || TEMP_SENSOR_TYPE == 22
  #include <DHTStable.h>
#elif TEMP_SENSOR_TYPE == 1820
  #include <OneWire.h>
  #include <DallasTemperature.h>
#endif
#include <PinChangeInterrupt.h>

// LCD & rotary encoder
#include <CMBMenu.hpp>
#include <LiquidCrystal_I2C.h>

// rotary encoder push button setup
typedef struct Buttons {
    const byte pin = ENCODER_PB_PIN;  // (change your pin in config file)
 
    unsigned long counter=0;
    bool prevState = HIGH;
    bool currentState;
} Button;

// version number of the software
#define SOFTWARE_VERSION_MAJOR 2
#define SOFTWARE_VERSION_MINOR 3
#define SOFTWARE_VERSION_PATCH 2

// version of the eeprom data model; must be increased if the data model changes
#define EEPROM_VERSION 5

// eeprom addresses
#define EEPROM_ADDR_VERSION  0 // 1 byte
#define EEPROM_ADDR_SETTINGS 1 // many bytes

// array for dynamic access to defined pins
const uint8_t valvePins[4] = { VALVE_0_PIN, VALVE_1_PIN, VALVE_2_PIN, VALVE_3_PIN };
const uint8_t buttonPins[4] = { VALVE_0_BUTTON_PIN, VALVE_1_BUTTON_PIN, VALVE_2_BUTTON_PIN, VALVE_3_BUTTON_PIN };
const uint8_t sensorAdcPins[4] = { SENSOR_0_ADC, SENSOR_1_ADC, SENSOR_2_ADC, SENSOR_3_ADC };


// structure of the settings stored in the eeprom and loaded at runtime
struct Settings {
  bool channelEnabled[4];      // indicator if the channel is enabled or not
  uint16_t adcTriggerValue[4]; // minimum adc value which will trigger the watering
  uint16_t wateringTime[4];    // watering time in seconds                          
  uint16_t checkInterval;      // adc check interval in seconds
  uint16_t humSensorInterval; // temperature sensor read interval in seconds
  bool sendAdcValuesThroughRH; // send all adc values through RadioHead or not
  bool pushDataEnabled;        // if data will be actively pushed by the system over RadioHead
  uint8_t serverAddress;       // the address of the server in the RadioHead network
  uint8_t ownAddress;          // the address of this node in the RadioHead network
  uint16_t delayAfterSend;     // time in milliseconds to delay after each data send
  int8_t humSwitchTriggerValue; // value where to trigger the temperature switch   
  uint8_t humSwitchHystTenth; // hysteresis of the humidty switch in tenth of the value (10 = 0,1)
  bool humSwitchInverted;     // if the switch will be inverted (default temp>value = on)
  
  //***************************** X light per Day*********************************
  // this was added to the original software in order to configure triggervalues etc. in Menu
  uint8_t lightMinTime;
  uint8_t lightTriggerValue;
};

/**
 * Macro to check the time for time-based events.
 * If a is greater than or equal to b this returns true, otherwise false.
 * This resprects a possible rollover of a and b.
 */
#define checkTime(a, b) ((long)(a - b) >= 0)


// global variables
extern Settings settings;

extern volatile bool channelTurnOn[4]; // volatile to use this inside a ISR
extern volatile unsigned long channelTurnOffTime[4];
extern unsigned long adcNextReadTime;
extern unsigned long humSensorNextReadTime;

extern uint16_t currentDayStart;
extern uint16_t lightTodayTime;

extern volatile bool channelOn[4];
extern uint16_t adcValues[4];
extern float temperature;
extern float humidity;

// *****************
//  Rotary encoder & LCD setup
// *****************
extern int pulseCount;                   // rotation step count
extern int SIG_A;                        // pin A output
extern int SIG_B;                        // pin B output
extern int SIG_PB;                       // push button output
extern int lastSIG_A;                    // last state of SIG_A
extern int lastSIG_B;                    // last state of SIG_B

unsigned short modVal;                   // capture encoder rotation if true

extern int *valPtr;                             // is passed to the corresponding actual value, points to the value user wants to modify
extern int *arrPtr;                             // is passed to RotateLeft/Right(), points to the array user wants to modify

extern int counter;                             // if 1 -> encoder turned or pressed -> refresh lcd
extern unsigned long previousMillis;        
extern const long interval;                     // check if pb pressed
extern unsigned long prevMi;
extern const long inter;                        // time before turn backlight off

extern const unsigned long  longPress;          // pb needs to be pressed for this long before mode change
extern int switchmode;                          // if 1 then mode = manual and screen turns on, 0 = automatic so we download hardcoded default settings
extern int refreshVal;                          // controls LCD refresh when selecting values for sensors

extern const int max = 10;
extern int rotate;                       // rotate through the arrays passed to RotateLeft() & RotateRight()

void RotateLeft(int array[]);            // functions to rotate arrays left & right
void RotateRight(int array[]);

extern LiquidCrystal_I2C lcd;            // set the LCD address to 0x27 for a 16 chars and 2 line display

// ** menu **
// create CMBMenu instance
// (here for maximum 100 menu entries)
extern CMBMenu<100> g_Menu;

// arrays to rotate through with preset values when manually changing settings
extern int adcTriggerValues[max];
extern int wateringTimeValues[max];
extern int humidityTriggerValues[max];
extern int lightTimeValues[max];

// ********************************************
// definitions
// ********************************************
// define text to display
const char MenuAdc_pc[] PROGMEM = {"1. ADC Trigger Value"};
const char MenuAdcReturn_pc[] PROGMEM = {"1.1 return"};
const char MenuAdcModify_pc[] PROGMEM = {"1.2 modify"};

const char MenuWater_pc[] PROGMEM = {"2. Watering Time"};
const char MenuWaterReturn_pc[] PROGMEM = {"2.1 return"};
const char MenuWaterModify_pc[] PROGMEM = {"2.1 modify"};

const char MenuHum_pc[] PROGMEM = {"3. Humidity Trigger Value"};
const char MenuHumReturn_pc[] PROGMEM = {"3.1 return"};
const char MenuHumModify_pc[] PROGMEM = {"3.1 modify"};

const char MenuLightTime_pc[] PROGMEM = {"4. Light per Day"};
const char MenuLightTimeReturn_pc[] PROGMEM = {"4.1 return"};
const char MenuLightTimeModify_pc[] PROGMEM = {"4.1 modify"};

const char MenuOk_pc[] PROGMEM = {"Save & Exit"};

// define function IDs
enum MenuFID {
  MenuDummy,

  MenuAdc,                     // main layer
  MenuAdcReturn,               // returns to previous
  MenuAdcModify,               // enters next layer

  MenuWater,
  MenuWaterReturn,
  MenuWaterModify,

  MenuHumidity,
  MenuHumReturn,
  MenuHumModify,

  MenuLightTime,
  MenuLightTimeReturn,
  MenuLightTimeModify,

  MenuOk                      // Save & Exit setup
};

enum RotaryEncoder {
  Idle,
  Left,
  Right,
  Enter
};

Button button;                // create encoder push button instance

// functions used to control menu
void ExitModify();
void MReturn();
void MenuAdcMod();
void MenuWaterMod();
void MenuHumMod();
void MenuLightMod();
void printMenuEntry(const char* f_Info);
void SaveSettings();

// *****************
//  Rotary encoder & LCD setup - END
// *****************

#if BAT_ENABLED == 1
  extern uint16_t batteryRaw;
#endif

extern bool humSwitchOn;
extern float humSwitchTriggerValueHigh;
extern float humSwitchTriggerValueLow;

extern bool pauseAutomatic;

#if TEMP_SENSOR_TYPE == 11 || TEMP_SENSOR_TYPE == 12 || TEMP_SENSOR_TYPE == 22
  extern DHTStable dhtSensor;
#elif TEMP_SENSOR_TYPE == 1820
  extern DallasTemperature ds1820;
#endif

#endif
