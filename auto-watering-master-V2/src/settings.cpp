/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 *
 * Settings and setting-handlers for the options which can be changed at runtime.
 */

#include "settings.h"

#include <EEPROM.h>

/**
 * Load the default settings.
 */
void loadDefaultSettings () {
  // set default values
  for (uint8_t chan = 0; chan < 4; chan++) {
    //**************************Channel 1 & 2 can be enabled here************************************* 
    settings.channelEnabled[chan] = (chan == 0)||(chan == 1)||(chan == 2); // should turn on chan 0, 1 & 2
    settings.adcTriggerValue[chan] = 512; // adc trigger value                                  #controlled in Menu
    settings.wateringTime[chan] = 5; // opening time in seconds                                 #controlled in Menu
  }
  settings.checkInterval = 120; // check interval - 2 minutes
  settings.humSensorInterval = 60; // temperature sensor read interval - 1 minute
  settings.sendAdcValuesThroughRH = false; // send all read adc values using through RadioHead
  settings.pushDataEnabled = false; // push data actively via RadioHead
  settings.serverAddress = RH_SERVER_ADDR; // RadioHead remote node address
  settings.ownAddress = RH_OWN_ADDR; // RadioHead address of this node
  settings.delayAfterSend = 10; // milliseconds to delay after each send
  settings.humSwitchTriggerValue = 60; // turn on temperature switch if > 30°C                  #controlled in Menu
  settings.humSwitchHystTenth = 50; // 2°C hysteresis -> 32°C on, 28°C off
  settings.humSwitchInverted = false; // don't invert - turn on if greater

  //************** these settings were added to the software *******************************************
  settings.lightMinTime = 120; // light per day in min                                   #controlled in Menu
  settings.lightTriggerValue = 180; // adc trigger Value default is ~150

  calchumSwitchTriggerValues();
}

/**
 * Load the stored settings from the eeprom.
 */
void loadSettings () {
  EEPROM.get(EEPROM_ADDR_SETTINGS, settings);

  calchumSwitchTriggerValues();
}

/**
 * Save the current settings into the eeporm.
 */
void saveSettings () {
  // write to eeprom
  EEPROM.put(EEPROM_ADDR_SETTINGS, settings);
}

/**
 * Calculate temperature switch high/low trigger values.
 */
void calchumSwitchTriggerValues () {
  humSwitchTriggerValueHigh = settings.humSwitchTriggerValue + (float)settings.humSwitchHystTenth/10;
  humSwitchTriggerValueLow = settings.humSwitchTriggerValue - (float)settings.humSwitchHystTenth/10;
}
