/*
 * Automatic Watering System
 *
 * (c) 2018-2021 Peter Müller <peter@crycode.de> (https://crycode.de)
 *
 * Handler functions for PCINTs triggered by pressed buttons.
 */

#include "pcint.h"

void handlePcintButton0 (void) {
  handlePcintButton(0);
}

void handlePcintButton1 (void) {
  handlePcintButton(1);
}

void handlePcintButton2 (void) {
  handlePcintButton(2);
}

void handlePcintButton3 (void) {
  handlePcintButton(3);
}

void handlePcintButton (uint8_t chan) {
  // check if the channel is on or off
  if (channelOn[chan]) {
    // turn off on next loop
    channelTurnOffTime[chan] = millis();
  } else {
    // turn on on next loop
    channelTurnOn[chan] = true;
  }
}

// *******
//  Rotary encoders interrupt service
// *******
void A_CHANGE() {                     // Interrupt Service Routine (ISR)
  detachInterrupt(0);                 // important
  SIG_A = digitalRead(ENCODER_A_PIN);         // read state of A
  SIG_B = digitalRead(ENCODER_B_PIN);         // read state of B
 
  if((SIG_B == SIG_A) && (lastSIG_B != SIG_B)) {
    pulseCount--;                     // anti-clockwise rotation
    lastSIG_B = SIG_B;
    if (modVal == 1){
      RotateLeft(arrPtr);
      refreshVal = 1;
    }else{
      g_Menu.left();
      counter = 1;
    }
  }
 
  else if((SIG_B != SIG_A) && (lastSIG_B == SIG_B)) {
    pulseCount++;                     // clockwise rotation
    lastSIG_B = SIG_B > 0 ? 0 : 1;    // save last state of B
    if (modVal == 1){
      RotateRight(arrPtr);
      refreshVal = 1;
    }else{
      g_Menu.right();
      counter = 1;
    }
  }
  attachInterrupt(digitalPinToInterrupt(ENCODER_A_PIN), A_CHANGE, CHANGE);
}