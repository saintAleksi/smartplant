[project]: visuals/smartPlantProject.jpg

# SmartPlant

-- Course project for Development & Testing of Prototypes -- written in C++

This repo contains the source code for a small greenhouse capable of adjusting lighting / watering / humidity based on sensor input

## Description
Our group set out to make a small greenhouse capable of adjusting the growing conditions based on sensor input.
A small LCD screen from LiquidCrystal is used to allow the user to input threshold values which are then saved to the mcu:s EEPROM.
We used an Arduino mega2560 because it provided us with enough HW pins and the device was easily obtainable at the time of writing.

While the project was a success and we managed to do what we set out to do, there are many ways this project could be expanded.

## Visuals
![project][project]

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment
__Project leader:__
* Niklas Häcker

__Software team:__
* Aleksi Perälä
* Max Waldbillig

__Construction team:__
* Janne Petäjä
* Teemu Nummelin

__Lights & Wiring:__
* Jonas Helm
* Corentin Speleers

__Water distribution:__
* Christian Stenger

__Report template:__
* Natalie Jouaneh

__Special acknowledgments:__
* This project relied heavily on the work of __Peter Müller__ (project base).

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

