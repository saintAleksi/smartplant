#include <Arduino.h>
#include <CMBMenu.hpp>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

// ********************************************
// MenuWithEncoder   // this works as a placeholder template for the actual one used in the project
//                      used to debug the rotary encoder and LCD
//                      this works out-of-the-box with PlatformIO
// ********************************************

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

// ** menu **
// create global CMBMenu instance
// (here for maximum 100 menu entries)
CMBMenu<100> g_Menu;

// ********************************************
// definitions
// ********************************************
// define text to display
const char MenuFoo_pc[] PROGMEM = {"1. Foo"};
const char MenuReturn_pc[] PROGMEM = {"1.1 return"};

const char MenuModify_pc[] PROGMEM = {"1.2 modify"};

const char MenuTest1_pc[] PROGMEM = {"1.2.1 Test1"};
const char MenuTest2_pc[] PROGMEM = {"1.2.2 Test2"};

const char MenuBar_pc[] PROGMEM = {"2. Bar"};
const char MenuBarReturn_pc[] PROGMEM = {"2.1 return"};
const char MenuBarA_pc[] PROGMEM = {"2.1 modify"};

const char MenuOk_pc[] PROGMEM = {"Save & Exit"};

// define function IDs
enum MenuFID {
  MenuDummy,
  MenuFoo,                  // main layer

  MenuReturn,               // returns to previous
  MenuModify,               // enters next layer

  MenuTest1,
  MenuTest2,
  MenuBar,
  MenuBarA,
  MenuBarReturn,

  MenuOk
};

enum RotaryEncoder {
  Idle,
  Left,
  Right,
  Enter
};

int mockVal1 = 0;                     // store user-defined sensor values to these placeholders
int mockVal2 = 0;
int mockVal3 = 0;

int *mockValPtr;                      // is passed to the corresponding actual value, points to the value user wants to modify

int modVal = 0;                       // capture encoder rotation if true
int *arrPtr;                          // is passed to RotateLeft/Right(), points to the array user wants to modify
const int max = 10;                   // max number of items

int exampleValue1[max] = {1,2,3,4,5,6,7,8,9,10};                  // array to rotate through when modifying sensor values etc.
int exampleValue2[max] = {10,20,30,40,50,60,70,80,90,100};        // preset values for the user to loop through
int exampleValue3[max] = {50,100,150,200,250,300,350,400,450,500}; 
int exampleValue4[max];                                           // could also just declare an empty array and let the user 
                                                                  // input values one by one

int i=0;                              // used to scroll through the values of the arrays

// ********************************************
// RotateLeft   // counter-clockwise rotation through the array
// ********************************************
void RotateLeft(int array[]){

  if (i < max && i >= 0){
    i--;
  }else{
    i = 9;
  }

  Serial.println(array[i]);
}

// ********************************************
// RotateRight   // clockwise rotation through the array
// ********************************************
void RotateRight(int array[]){

  if (i < max && i >= 0){
    i++;
  }else{
    i = 0;
  }

  Serial.println(array[i]);
}

// ********************************************
// ExitModify   // exits the current modification & changes modVal to 0 -> rotation input is restored to main menu cycling  
// ********************************************
void ExitModify(){
  g_Menu.exit();
  modVal = 0;

  Serial.println(mockVal2);
}

// ********************************************
// FooA
// ********************************************
void FooA()
{
  Serial.println("Function FooA() was called.");
  lcd.println("Function FooA was called");
}

// ********************************************
// MReturn  -> return to previous menu layer
// ********************************************
void MReturn(){
  g_Menu.exit();
}

// ********************************************
// Test1
// ********************************************
void Test1()
{
  Serial.println("Function Test1() was called.");
  lcd.println("Function Test1 was called");
}

// ********************************************
// Test2
// ********************************************
void Test2()
{
  Serial.println("Function Test2() was called.");
  lcd.println("Function Test2 was called");
}

// ********************************************
// BarA
// ********************************************
void BarA()
{
  Serial.println("Function BarA() was called.");
  lcd.println("Function BarA was called");
  modVal = 1;
  arrPtr = exampleValue2;
  //mockValPtr = &mockVal2;
  mockVal2 = *mockValPtr;
  Serial.println(modVal);
}

// ********************************************
// ** menu **
// printMenuEntry
// ********************************************
void printMenuEntry(const char* f_Info)
{
  String info_s;
  MBHelper::stringFromPgm(f_Info, info_s);

  // when using LCD: add/replace here code to
  // display info on LCD
  Serial.println("----------------");
  lcd.clear();
  lcd.println(info_s);
  Serial.println("----------------");
}

// ------------ rotary encoder

int pulseCount;                       // rotation step count
int SIG_A = 0;                        // pin A output
int SIG_B = 0;                        // pin B output
int SIG_PB = 0;                       // push button output
int lastSIG_A = 0;                    // last state of SIG_A
int lastSIG_B = 0;                    // last state of SIG_B

// (change your pins here)
int Pin_A = 2;                        // interrupt pin (digital) for A
int Pin_B = 3;                        // interrupt pin (digital) for B

typedef struct Buttons {              // push button setup
    const byte pin = 18;              // (change your pin here)
    const int debounce = 10;
 
    unsigned long counter=0;
    bool prevState = HIGH;
    bool currentState;
} Button;

// create a Button variable type
Button button;

int counter = 0;                      // if 1 -> encoder turned or pressed -> refresh lcd
unsigned long previousMillis = 0;        
const long interval = 405;            // check if pb pressed
unsigned long prevMi = 0;
const long inter = 30000;            // time before turn backlight off

const unsigned long  longPress = 5000;// pb needs to be pressed for this long before mode change
int switchmode = 0;                   // if 1 then mode = manual and screen turns on, 0 = automatic so we download hardcoded default settings
int refreshVal = 0;                   // controls LCD refresh when selecting values for sensors

void A_CHANGE() {                     // Interrupt Service Routine (ISR)
  detachInterrupt(0);                 // important
  SIG_A = digitalRead(Pin_A);         // read state of A
  SIG_B = digitalRead(Pin_B);         // read state of B
 
  if((SIG_B == SIG_A) && (lastSIG_B != SIG_B)) {
    pulseCount--;                     // anti-clockwise rotation
    lastSIG_B = SIG_B;
    if (modVal == 1){
      RotateLeft(arrPtr);
      refreshVal = 1;
    }else{
      g_Menu.left();
      counter = 1;
    }
    Serial.print(pulseCount);
    Serial.println(" - Reverse");
  }
 
  else if((SIG_B != SIG_A) && (lastSIG_B == SIG_B)) {
    pulseCount++;                     // clockwise rotation
    lastSIG_B = SIG_B > 0 ? 0 : 1;    // save last state of B
    if (modVal == 1){
      RotateRight(arrPtr);
      refreshVal = 1;
    }else{
      g_Menu.right();
      counter = 1;
    }
    Serial.print(pulseCount);
    Serial.println(" - Forward");
  }
  attachInterrupt(digitalPinToInterrupt(Pin_A), A_CHANGE, CHANGE);
}

// ----------------

void setup() {
  SIG_B = digitalRead(Pin_B);         // current state of B
  SIG_A = SIG_B > 0 ? 0 : 1;          // let them be different
  // attach iterrupt for state change, not rising or falling edges
  attachInterrupt(digitalPinToInterrupt(Pin_A), A_CHANGE, CHANGE);
  
  pinMode(button.pin, INPUT_PULLUP);  // declare pushbutton as input
  lcd.init();                         // initialize the lcd 
  lcd.backlight();
  Serial.begin(9600);
  Serial.println("l: left, r: right, e: enter, x: return, m: print menu");

  // ** menu **
  // add nodes to menu (layer, string, function ID)
  g_Menu.addNode(0, MenuFoo_pc , MenuFoo);
  g_Menu.addNode(1, MenuReturn_pc, MenuReturn);
  g_Menu.addNode(1, MenuModify_pc, MenuModify);
  //g_Menu.addNode(2, MenuTest1_pc, MenuTest1);
  //g_Menu.addNode(2, MenuTest2_pc, MenuTest2);

  g_Menu.addNode(0, MenuBar_pc, MenuBar);
  g_Menu.addNode(1, MenuBarReturn_pc, MenuBarReturn);
  g_Menu.addNode(1, MenuBarA_pc, MenuBarA);

  g_Menu.addNode(0, MenuOk_pc, MenuOk);


  // ** menu **
  // build menu and print menu
  // (see terminal for output)
  const char* info;
  g_Menu.buildMenu(info);
  g_Menu.printMenu();

  // ** menu **
  // print current menu entry
   printMenuEntry(info);
}

void loop() {

  if (switchmode == 0){
    
    // check the button
    button.currentState = digitalRead(button.pin);

    if (button.currentState != button.prevState) {
      //delay(button.debounce);
      // update status in case of bounce
      button.currentState = digitalRead(button.pin);
      if (button.currentState == LOW) {
          // a new press event occured
          // record when button went down
          button.counter = millis();
      }
      if (button.currentState == HIGH) {
          // but no longer pressed, how long was it down?
          unsigned long cMillis = millis();

          if ((cMillis - button.counter >= longPress)) {
              // the long press was detected
              switchmode = 1;
          }
      }
      // used to detect when state changes
      button.prevState = button.currentState;
      Serial.println(switchmode);
    }
  } 

  if (switchmode == 1){
    int fid = 0;                         // function ID
    const char* info;                    // info text from menu
    bool layerChanged=false;             // go to deeper or upper layer?
    SIG_PB = digitalRead(button.pin);
    unsigned long currentMillis = millis();
    
    // check how long has it been since last user input
    // if longer than set interval, turn backlight off to save power
    if (currentMillis - prevMi >= inter){
      prevMi = currentMillis;
      lcd.noBacklight();
      counter = 0;
    }

    // if changing a value, display current array value on LCD
    // LCD doesnt work well with interrupts so thats why we are doing the displaying here
    if (modVal == 1){
      prevMi = currentMillis;           // update so screen doesnt enter power save mode

      if (refreshVal == 1){
      lcd.clear();
      lcd.println(arrPtr[i]);
      refreshVal = 0;                   // swap to false so we wait for next input
      }                                 // and wont refresh the screen continuously

      if (SIG_PB == LOW && currentMillis - previousMillis >= interval){
        mockValPtr = &arrPtr[i];
        ExitModify();
      }
      Serial.println(*mockValPtr);
    }

    //// ** menu **
    //// print/update menu
    //// and get current function ID "fid"
    if (counter == 1){
        fid = g_Menu.getInfo(info);
        printMenuEntry(info);
        lcd.backlight();
        prevMi = currentMillis;
        counter = 0;
    }

    // check if pb pressed and then update mode info
    if (currentMillis - previousMillis >= interval){
      previousMillis = currentMillis;

      if(SIG_PB == LOW){
      fid = g_Menu.getInfo(info);
      Serial.println("pressed");
      g_Menu.enter(layerChanged);
      counter = 1;
      }

    }

    if ((0 != fid) && (counter == 1) && (!layerChanged)) {
      switch (fid) {
        case MenuReturn:
          MReturn();
          break;
        case MenuModify:
        
          break;
        case MenuOk:
          // call EEPROM save function and set switchmode to = 0 -> ie automatic
          break;
        case MenuBarReturn:
          MReturn();
          break;
        case MenuBarA:
          BarA();
          break;
        case MenuTest1:
          Test1();
          break;
        case MenuTest2:
          Test2();
          break;
        default:
          break;
      }
    }
  } 
}